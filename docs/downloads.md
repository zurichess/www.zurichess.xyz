# Neuchatel (stable)

Version | Platform | SHA224
------- | -------- | ------
[zurichess-neuchatel-darwin-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-neuchatel-darwin-amd64)           | darwin,amd64  | ac43430328c3ea4688bc5a672172bb1c89feb38988616fd76fe71c7b
[zurichess-neuchatel-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-neuchatel-linux-386)                 | linux,386     | fed3533db59a02c69ebc148c2eaaaed261647dd88d1ffe0047d22292
[zurichess-neuchatel-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-neuchatel-linux-amd64)             | linux,amd64   | 1dd1e3166c322dc060cc2cdd49321afa44ddc2933d1385b5ca401f9d
[zurichess-neuchatel-linux-arm](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-neuchatel-linux-arm)                 | linux,arm     | 007bea149c14b8bbf846be1b2ef958f82fbc912d6c80208f593bd3e3
[zurichess-neuchatel-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-neuchatel-windows-386.exe)     | windows,386   | e9f2907a9bce75587559daf07c3e07d25c42fd3bd8be6ac0005bee2e
[zurichess-neuchatel-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-neuchatel-windows-amd64.exe) | windows,amd64 | b80d2d562b1c30c10cabc44b34e32a994edd5588b949092c38207696

# Luzern

Version | Platform | SHA224
------- | -------- | ------
[zurichess-luzern-darwin-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-luzern-darwin-amd64)                      |  darwin,amd64   |  f358248ed1e213593908ba2eec21c4b9b043a984c5c791ba8f75f8ed
[zurichess-luzern-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-luzern-linux-386)                            |  linux,386      |  a04fe93cb8750424a47df682e5c215761bd49e76292fe89afcef964f
[zurichess-luzern-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-luzern-linux-amd64)                        |  linux,amd64    |  e472c4a85097bd427d3348e774b91155a89fc558473008c0224dc29f
[zurichess-luzern-linux-arm](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-luzern-linux-arm)                            |  linux,arm      |  210a2b639eaf29ea09d54fab1f8b8c07a2cdc7f82b9afc7f15a78d05
[zurichess-luzern-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-luzern-windows-386.exe)                |  windows,386    |  5c4c419c4f1b11190eae7beecf2bf32825b01479a5ad896a2afcf192
[zurichess-luzern-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-luzern-windows-amd64.exe)            |  windows,amd64  |  be57372a4542e09299d91ec14f1827c0180462fb0adf392b9de56b12

# Jura

Version | Platform | SHA224
------- | -------- | ------
[zurichess-jura-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-jura-linux-amd64)                            |  linux,amd64    |  c9a641b4085d0a18f2112bf01b72971ec06bd9e16d467e8988313189
[zurichess-jura-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-jura-linux-386)                                |  linux,386      |  3bd82d95c77c238cc8c84d52546e8cbee80f4216692609f8c38bcbc7
[zurichess-jura-linux-arm](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-jura-linux-arm)                                |  linux,arm      |  55d72fe6988ff5b8f5ba5ba2d4a84ca9b82682288f54e7a36c72bcdc
[zurichess-jura-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-jura-windows-amd64.exe)                |  windows,amd64  |  db3fd2e7c05ec3e4e008d7a38c9f0055349552b1b014dbcfa71cce6b
[zurichess-jura-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-jura-windows-386.exe)                    |  windows,386    |  d0fea16f1f59eb27545729d35caea454b6384fd611f8ee66991af485

# Graubuenden

Version | Platform | SHA224
------- | -------- | ------
[zurichess-graubuenden-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-graubuenden-linux-amd64)              |  linux,amd64    |  f5fbf70fdbf0d750a45cdec48230501d1909170e8ad2c0d929991cc1
[zurichess-graubuenden-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-graubuenden-linux-386)                  |  linux,386      |  74c1295949ea7b5746a29ce73d344e8771e5c6f80351c06c27eb9504
[zurichess-graubuenden-linux-arm](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-graubuenden-linux-arm)                  |  linux,arm      |  8f9c62f7abd9074c9d14c13f6da6e1f556bfef72a62d700ac8c52ef8
[zurichess-graubuenden-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-graubuenden-windows-amd64.exe)  |  windows,amd64  |  759d44240d19e0cef31844d6b6823b1e256b68801a952cf434d8d9fb
[zurichess-graubuenden-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-graubuenden-windows-386.exe)      |  windows,386    |  f044a1d5c604a98e88790826f18506d30c6a9c756b3b2bcd081260bc

# Glarus

Version | Platform | SHA224
------- | -------- | ------
[zurichess-glarus-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-glarus-windows-amd64.exe)            |  windows,amd64  |  13c3bd2053740633538db5e43723b136bd8f3465cc7b651fe8f1bc8e
[zurichess-glarus-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-glarus-windows-386.exe)                |  windows,386    |  24106b9e805c10ddbd485011cf1caee714861e3e3f2110ffd8009bf7
[zurichess-glarus-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-glarus-linux-amd64)                        |  linux,amd64    |  c5853b30fd637652f07a4a6bdc2188e5ebb93fe9acefaa0e188d4afa
[zurichess-glarus-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-glarus-linux-386)                            |  linux,386      |  ec825583761bc5092e0bf9e4f6c0a9c5e59fe643905d1cbbcb7612f5

# Geneva

Version | Platform | SHA224
------- | -------- | ------
[zurichess-geneva-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-geneva-windows-amd64.exe)            |  windows,amd64  |  b48e20e04ec6c8f549d196d2b5f9c201e6fdb274ea4967fafd132bff
[zurichess-geneva-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-geneva-windows-386.exe)                |  windows,386    |  784de38e9abba47ac1ed626aa0b52138433e7150ac7bf33719e947d6
[zurichess-geneva-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-geneva-linux-amd64)                        |  linux,amd64    |  9b0b4a78b07fcdddafb54c979f1dcb84a01024a443e6e605ff547356
[zurichess-geneva-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-geneva-linux-386)                            |  linux,386      |  174c835ed42f61bf7d54bde8667bcec3f6a71885a45ac1ea344f0f38

# Fribourg

Version | Platform | SHA224
------- | -------- | ------
[zurichess-fribourg-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-fribourg-windows-amd64.exe)        |  windows,amd64  |  d1cc1db7223b0aa1793284b4bace3e2d5165b191e9d1ff4cb710da3d
[zurichess-fribourg-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-fribourg-windows-386.exe)            |  windows,386    |  9940208c1bb83f2480492dd58699e1f78e27b58268fdd67bd7e4f5f0
[zurichess-fribourg-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-fribourg-linux-amd64)                    |  linux,amd64    |  e2fcecf6ba1e9adf4bced2a14135e3e027e21cd45ffdeab3ca7be731
[zurichess-fribourg-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-fribourg-linux-386)                        |  linux,386      |  06fd06d3b8710ae60b4d3e8bdda522206f88a776b3ea709acceff1c9

# Bern

Version | Platform | SHA224
------- | -------- | ------
[zurichess-bern-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-bern-windows-amd64.exe)                |  windows,amd64  |  8835d525bc3657310eefab51b2d123c965b8a74bc398e267d013ccab
[zurichess-bern-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-bern-windows-386.exe)                    |  windows,386    |  3030b43ddd55026eac807df2067fb7354701f298484755b6da29b320
[zurichess-bern-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-bern-linux-amd64)                            |  linux,amd64    |  5ad52f0deb35986a095c212240f96a7c9075b6ae22389ffcc1e5279c
[zurichess-bern-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-bern-linux-386)                                |  linux,386      |  21191103894c0c7f9214688c2a7cec2aec9d6a71f51d3764600d4524

# Basel

Version | Platform | SHA224
------- | -------- | ------
[zurichess-basel-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-basel-windows-amd64.exe)              |  windows,amd64  |  f72109cf60541f6831ab3f311db29091acffed443ca052505fc6c4f5
[zurichess-basel-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-basel-windows-386.exe)                  |  windows,386    |  8b6aaa8ef112edbe9fef343133cd6baf241fd89371ae42743546dbb0
[zurichess-basel-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-basel-linux-amd64)                          |  linux,amd64    |  341cee48e50b3d0f37b6802c279a75ec6d583f527006fc412ef1b582
[zurichess-basel-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-basel-linux-386)                              |  linux,386      |  f5830e3a6b186fb7d45e5fdac97b4e44ddab67384c79d9da5f4e96b4

# Appenzeller

Version | Platform | SHA224
------- | -------- | ------
[zurichess-appenzeller-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-appenzeller-windows-amd64.exe)  |  windows,amd64  |  c2fa4d961822b0063df261b5cf4911dd3ff5042b973477f9234a99d2
[zurichess-appenzeller-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-appenzeller-windows-386.exe)      |  windows,386    |  fad0315dd9992ad1fd0024b808ae7649952c8e812fd3c487088a97ce
[zurichess-appenzeller-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-appenzeller-linux-amd64)              |  linux,amd64    |  8b13c0b36ed9b9ddd32f86b84b2e532f39e969594506c0e33483b1a6
[zurichess-appenzeller-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-appenzeller-linux-386)                  |  linux,386      |  066ef4c7153ef5fbcdefc6877dfdbab87921e3530fe5e7b323941505

# Aargau

Version | Platform | SHA224
------- | -------- | ------
[zurichess-aargau-windows-amd64.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-aargau-windows-amd64.exe)            |  windows,amd64  |  8a0a56e81bbb6fa3cc4173fe58c9674fab4468701868393189a25884
[zurichess-aargau-windows-386.exe](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-aargau-windows-386.exe)                |  windows,386    |  409b208e9b41103abea2a7e79fd921aa462ce88daf504fc2e77884f4
[zurichess-aargau-linux-amd64](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-aargau-linux-amd64)                        |  linux,amd64    |  9147e418a483dae62f5ae24d18cd4b0173356e2fd0bb4c5ec92a6de1
[zurichess-aargau-linux-386](https://bitbucket.org/zurichess/zurichess/downloads/zurichess-aargau-linux-386)                            |  linux,386      |  691880f9e9d1587b59be5c0ddbcab20c7522da39c3ed46ef340933d3
