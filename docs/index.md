# About

**Zurichess** is an open source chess engine.
Its current strength is around 2820 Elo on [CCRL
40/40](www.computerchess.org.uk/ccrl/4040/index.html) which is
around 2700 Elo on a regular human scale.  The engine supports
adjustable strength so the average chess player can still beat it.

# Playing

You can play online against Zurichess on the [Free Internet Chess
Server](http://www.freechess.org/).

For offline playing, you can [download](downloads.md) a binary
which is suitable for your platform and use it with a chess GUI. I
recommend [Xboard](https://www.gnu.org/software/xboard/) for Linux or
[Arena](http://www.playwitharena.com/) for Windows.

The latest stable version is available as an Archlinux AUR package at
[aur.archlinux.org/packages/zurichess/](https://aur.archlinux.org/packages/zurichess/).


# Testimonials

*I like it a lot (a unique human-like playing style)!* - Brendan, [Chess&Cognac](http://www.chessncognac.com).

# Get involved

Zurichess is an open source chess engine which means that you can read
the code, modify it or use it in your own projects. The engine is written
in [Go](https://www.golang.org) and can be used as a library for board
[setup](https://godoc.org/bitbucket.org/zurichess/zurichess/board), FEN
and EPD [parsing](https://godoc.org/bitbucket.org/zurichess/notation),
or position
[evaluation](https://godoc.org/bitbucket.org/zurichess/zurichess/engine).

Before sending your first patch see the contribution
[guidelines](https://bitbucket.org/zurichess/zurichess/src/master/CONTRIBUTING.md).

For feature requests and bug reports,
check out the source repository at
[bitbucket.org/zurichess/zurichess](https://bitbucket.org/zurichess/zurichess/overview).


# Contact

For project related questions contact the main author at
<a href="http://www.google.com/recaptcha/mailhide/d?k=014NTvFU5pFcstulAjMQ64Vg==&amp;c=aZoWKOW4yXbF3wNa6GX1HCWmHrdMTVJ8Zmy-dPxOitw=" onclick="window.open('http://www.google.com/recaptcha/mailhide/d?k\x3d014NTvFU5pFcstulAjMQ64Vg\x3d\x3d\x26c\x3daZoWKOW4yXbF3wNa6GX1HCWmHrdMTVJ8Zmy-dPxOitw\x3d', '', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=300'); return false;" title="Reveal this e-mail address">b...@gmail.com</a>.
For general chess programming questions please use a
dedicated discussion forum such as [TalkChess](http://www.talkchess.com).
